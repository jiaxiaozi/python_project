# coding=utf-8
from bs4 import BeautifulSoup           #网页解析，获取数据
import re                                             #正则表达式，进行文字匹配
import urllib.request,urllib.error          #指定url，获取网页数据
import xlwt                                         #进行excel操作
import sqlite3                                      #进行sqllite数据库操作
def main():
    baseurl ="https://movie.douban.com/top250?start="
    #1.爬取网页
    datalist = getData(baseurl)
    # savepath = "豆瓣电影top250.xls"
    dbpath = "movie.db"
    #3.保存数据
    # saveData(datalist ,savepath)
    saveData2DB(datalist,dbpath)
    # askURL(baseurl)

#正则表达式规则 *表示多个字符，？表示0个到多个
#影片详情链接的规则
findLink = re.compile(r'<a href="(.*?)">') #创建正则表达式对象，建立规则
#影片图片的链接
#re.S   让换行符包含在字符中
findImgSrc = re.compile(r'<img .*src="(.*?)"',re.S) #创建正则表达式对象，建立规则
#影片片名
findTitle = re.compile(r'<span class="title">(.*)</span>')
#影片评分
findRating = re.compile(r'<span class="rating_num" property="v:average">(.*)</span>')
#评分人数
findJudge = re.compile(r'<span>(\d*)人评价</span>')
#找到评价
findInq = re.compile(r'<span class="inq">(.*)</span>')
#找到影片的相关内容
findBd = re.compile(r'<p class="">(.*?)</p>',re.S)

#爬取网页
def getData(baseurl):
    datalist = [ ]
    for i in range(0,10):           #重复调取页面10次，观察规律发现，每次会在url后面+25，跳转到下一页
        url=baseurl+str(i*25)
        html = askURL(url)         #保存获取到的网页源码

    # 2.解析数据
        soup = BeautifulSoup(html, "html.parser")
        #class加下划线表示属性值
        for item in soup.find_all('div',class_="item"):    #查找符合要求的字符串，形成列表
            #print(item)   #测试查看电影全部信息
            data = [ ]
            item = str(item)
            # print(item)
            # break

            #link为获取影片的超链接
            link = re.findall(findLink, item)[0]            #[0]表示两个里面的第一个，排除重复的链接
            data.append(link)                                     #添加链接

            imgSrc = re.findall(findImgSrc, item)[0]
            data.append(imgSrc)                                 #添加图片

            titles = re.findall(findTitle, item)
            if(len(titles) == 2):
                ctitle = titles[0]
                data.append(ctitle)                                 #添加中文名
                otitle = titles[1].replace("/","")              #去掉无关符号
                # otitle_2 = re.sub("'\xa0"," ",otitle)
                # print(ctitle)
                data.append(otitle)                                 #添加外文名
            else:
                data.append(titles[0])
                data.append('          ')                               #外国名字留空
            rating = re.findall(findRating, item)[0]
            data.append(rating)                                     #添加评分

            judgeNum = re.findall(findJudge, item)
            data.append(judgeNum)                               #添加评价人数

            inq = re.findall(findInq, item)
            if len(inq) !=0:
                inq = inq[0].replace("。", "")                       #去掉句号
                data.append(inq)                                          #添加概述
            else:
                data.append(" ")                                            #留空

            bd = re.findall(findBd,item)[0]
            bd = re.sub('<br(\s+)?/>(\s+)?'," ",bd)               #去掉<br/>
            bd = re.sub('/'," ",bd)                                         #替换/
            bd = re.sub("\xa0", " ", bd)                                 #去掉人名后面的\xa0

            data.append(bd.strip())                                        #去掉前后的空格

            datalist.append(data)                                           #把处理好的一部电影信息放到datalist中去

    # print(datalist)
    return datalist

#得到一个指定的URL的网页内容
def askURL(url):
    #模拟浏览器头部消息，向豆瓣浏览器发送消息
    header={"user-agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36"}
    request = urllib.request.Request(url, headers=header)
    html=""
    try:
        response = urllib.request.urlopen(request)
        html = response.read().decode("utf-8")
       # print(html)
    except urllib.error.URLError as e:
        if (hasattr(e,"code")):
            print(e.code)
        if (hasattr(e, "reason")):
            print(e.reason)
    return html

#保存数据
def saveData(datalist,savepath):
    # 创建book对象
    book = xlwt.Workbook(encoding="utf-8",style_compression=0)
    # 创建工作表
    sheet = book.add_sheet("豆瓣电影Top250",cell_overwrite_ok=True)
    col = ("电影详情链接","电影封面链接","影片中文名称","影片外文名称","评分","评分数","概况","相关信息")
    for i in range(0,8):
        #col[i]代表列名
        sheet.write(0,i,col[i])
    for i in range(0,250):
        print("第%d条"%i)
        data = datalist[i]
        for j in range(0,8):
            #向表中写入数据
            sheet.write(i+1,j,data[j])
    book.save(savepath)


def saveData2DB(datalist,dbpath):
    init_db(dbpath)
    conn = sqlite3.connect(dbpath)
    cur = conn.cursor()

    for data in datalist:
        for index in range(len(data)):
            if index ==5:
                data[index] = '"' + str(data[index]) + '"'
                continue
            data[index] = '"'+data[index]+'"'
        sql ='''
                        insert into movie250 (
                        info_link, pic_link, cname, ename, score, rated, instroduction, info)
                        values(%s)'''%",". join(data)
                         #values(%s)'''%",". join('%s' %a for a in data)
        print(sql)
        cur.execute(sql)
        conn.commit()
    cur.close()
    conn.close()
                        
                        

            



def init_db(dbpath):
    #创建数据表
    sql ="""
            create table movie250
            (
                id integer primary key autoincrement,
                info_link text ,
                pic_link text ,
                cname varchar,
                ename varchar,
                score numeric,
                rated numeric,
                instroduction text ,
                info text
            
            
            )
            """

    conn = sqlite3.connect(dbpath)
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()
    conn.close()



if __name__ == "__main__":
    main()
    # init_db("movietest.db")
    print("爬取完毕！")