# -*- codeing =utf-8 -*-
# @Time : 2020/12/5 20:44
# @Author : 三千xc
# @File : wechat.py.py
# @Software: PyCharm
import sqlite3

from flask import Flask,render_template
import datetime

app = Flask(__name__)

# 路由解析，跳转主页
@app.route('/')
def index():
    return render_template("index.html")


@app.route('/index')
def home():
    # return render_template("index.html")
    return  index()

# 跳转电影页面
@app.route('/movie')
def movie():
    datalist = [ ]
    con = sqlite3.connect("movie.db")
    cur = con.cursor()
    sql = "select * from movie250"
    data = cur.execute(sql)
    for item in data:
        datalist.append(item)
    cur.close()
    con.close()
    return render_template("movie.html",movies = datalist)

# 跳转评分页面
@app.route('/score')
def score():
    #评分
    score = [ ]
    #评分相同的电影数量
    num = [ ]
    con = sqlite3.connect("movie2.db")
    cur = con.cursor()
    sql = "select score,count(score) from movie100 group by score"
    data = cur.execute(sql)
    for item in data:
        score.append(str(item[0]))
        num.append(item[1])
    cur.close()
    con.close()
    return render_template("score.html",score = score,num = num)

# 跳转关于页面
@app.route('/team')
def team():
    return render_template("team.html")

# 跳转词云页面
@app.route('/word')
def word():
    return render_template("word.html")



'''
#路由解析，通过用户访问相应的路径，匹配相应的函数
# @app.route('/')
# def hello_world():
#     return 'Hello World！！！！！！！！！222222！！!'

#通过访问路径，获取用户的字符串参数
@app.route("/user/<name>")
def welcome(name):
    return "你好，%s"%name

#通过访问路径，获取用户的整型参数
@app.route("/user/<int:id>")
def welcome2(id):
    return "你好，%d 好"%id

# 路由路径不能重复，用户通过唯一的路径来访问特定的函数


# 返回给用户渲染后的网页文件
# @app.route("/")
# def index2():
#     return  render_template("temp.html")

# 向页面传递变量
@app.route("/")
def index3():
    time = datetime.date.today()
    num = ["1","2","3"]
    task = {"23":"45","67":"89"}
    return  render_template("temp.html",var = time,list = num,task = task)
'''






# 开启debug模式
if __name__ == '__main__':
    app.run()

